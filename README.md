# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **The Knapsack Problem**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/gA0I_tFBCTE/0.jpg)](https://www.youtube.com/watch?v=gA0I_tFBCTE "Click to Watch")


# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.